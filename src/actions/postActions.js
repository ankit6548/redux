import {FETCH_POSTS, NEW_POST} from './types';

export function fetchPosts(){
    return function(dispatch) {
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(posts => dispatch({
            type:FETCH_POSTS,
            payload : posts
        }));   
    }
}

export function createPost(postData){
    return function(dispatch) {
        fetch('https://jsonplaceholder.typicode.com/posts',{
            method : 'POST',
            headers : {
                'content-type' : 'application/json'
            },
            body : JSON.stringify(postData)
        })
        .then(response => response.json())
        .then(post => dispatch({
            type:NEW_POST,
            payload : post
        }));  
    }
}

export function changeModel(modelName){
    return function(dispatch) {
        dispatch({
            type:"model_state_change",
            payload : modelName
        })
    }
}