import React, { Component } from 'react';
import propTypes from 'prop-types';
import {changeModel} from '../actions/postActions';
import { connect } from 'react-redux';

class SearchByOther extends Component {
    state = {
      teams: [],
      selectedModel: "",
      validationError: "",
      modelarr : [
          {
            id:1,
            value : "hyundai",
            display : "Hyundai"
          },{
            id:2,
            value : "bmw",
            display : "BMW"
          }
      ]
    }
  
    componentDidMount() {
          this.setState({ teams: [{value: '', display: 'Model'}].concat(this.state.modelarr) });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.model){
            this.setState({ selectedModel: nextProps.model });
        }
    }
  
    render() {
      return (
        <div>
          <select value={this.state.selectedModel} 
                  onChange={(e) => this.setState({selectedModel: e.target.value, validationError: e.target.value === "" ? "You must select your favourite team" : ""})}>
            {this.state.teams.map((team) => <option key={team.value} value={team.value}>{team.display}</option>)}
          </select>
          <div style={{color: 'red', marginTop: '5px'}}>
            {this.state.validationError}
          </div>
        </div>
      )
    }
  }

 
const mapStateToProps = state => ({
    model : state.posts.model
})
export default connect(mapStateToProps, {changeModel})(SearchByOther);