import React, { Component } from 'react';
import propTypes from 'prop-types';
import {changeModel} from '../actions/postActions';
import { connect } from 'react-redux';

// class SearchByModelDropDown extends Component {
//     constructor(){
//         super();
//         this.state = {
//             displayMenu: false,
//         };

//         this.showDropdownMenu = this.showDropdownMenu.bind(this);
//         this.hideDropdownMenu = this.hideDropdownMenu.bind(this);

//     };

//     showDropdownMenu(event) {
//         event.preventDefault();
//         this.setState({ displayMenu: true }, () => {
//         document.addEventListener('click', this.hideDropdownMenu);
//         });
//         }

//         hideDropdownMenu() {
//         this.setState({ displayMenu: false }, () => {
//             document.removeEventListener('click', this.hideDropdownMenu);
//         });

//         }

//     render() {
//         return (
//             <div>
//                 {/* <div style = {{background:"red",width:"200px"}} className="button" onClick={this.showDropdownMenu}> Search by model </div> */}
//                 <select style = {{background:"red",width:"200px"}} value={this.state.selectedTeam} onClick={this.showDropdownMenu}
//                     onChange={(e) => this.setState({selectedModel: e.target.value})}>
//                 { this.state.displayMenu ? (
                
//                     <option value="0">Please select</option>
//                     <option key="hyundai" value="hyundai">Hyundai</option>
//                     <option key="bmw" value="bmw">BMW</option>
//             ):
//             (
//                 null
//             )
//             }

//             </select>
//             </div>
//         );
//     }
// }

// export default SearchByModelDropDown;

class SearchByModelDropDown extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            teams: [],
            selectedModel: "",
            validationError: "",
            modelarr : [
                {
                id:1,
                value : "hyundai",
                display : "Hyundai"
                },{
                id:2,
                value : "bmw",
                display : "BMW"
                }
            ]
         };
         this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({
            selectedModel:e.target.value
        }, () => {
            this.props.changeModel(this.state.selectedModel);
        });
    }
    componentDidMount() {
          this.setState({ teams: [{value: '', display: 'Search By Model'}].concat(this.state.modelarr) });
    }
  
    render() {
      return (
        <div>
          <select value={this.state.selectedTeam} 
                  onChange={this.onChange}>
            {this.state.teams.map((team) => <option key={team.value} value={team.value}>{team.display}</option>)}
          </select>
          <div style={{color: 'red', marginTop: '5px'}}>
            {this.state.validationError}
          </div>
        </div>
      )
    }
  }

SearchByModelDropDown.propTypes = {
    changeModel : propTypes.func.isRequired,
}

  export default connect(null, {changeModel})(SearchByModelDropDown);