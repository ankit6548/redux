import React, { Component } from 'react';
import propTypes from 'prop-types';
import {fetchPosts} from '../actions/postActions';
import { connect } from 'react-redux';
class Posts extends Component {

    componentWillMount() {
        this.props.fetchPosts();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.newPost){
            this.props.posts.unshift(nextProps.newPost);
        }
    }
    render() {
        const postItems = this.props.posts.map(post => (
            <div key={post.id}>
                <p>{post.id}</p>
                <p>{post.title}</p>
            </div>
        ));
        return (
            <div>
                <h1>Posts</h1>
                {postItems}
            </div>
        );
    }
}

Posts.propTypes = {
    fetchPosts : propTypes.func.isRequired,
    posts : propTypes.array.isRequired,
    newPost : propTypes.object
}
const mapStateToProps = state => ({
    posts: state.posts.items,
    newPost : state.posts.item
})
export default connect(mapStateToProps, {fetchPosts})(Posts);