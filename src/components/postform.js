import React, { Component } from 'react';
import propTypes from 'prop-types';
import {createPost} from '../actions/postActions';
import { connect } from 'react-redux';
class PostForm extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            post : {
                title : '',
                body : ''
            }
         };

         this.onChange = this.onChange.bind(this);
         this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e){
        this.setState({[e.target.name] : e.target.value})
    }

    onSubmit(e){
        e.preventDefault();
        const post = {
            title : this.state.title,
            body : this.state.body
        }
        this.props.createPost(post);
    }

    render() {
        return (
            <form onSubmit={this.onSubmit} >
                <label>Title:</label>
                <input type="text" name="title" value={this.state.title} id="title" onChange = {this.onChange} />
                <br />
                <label>Body:</label>
                <textarea type="text" name="body" value={this.state.body} id="body" onChange = {this.onChange} />
                <br />
                <button type="submit">Submit</button>
            </form>            
        );
    }
}

PostForm.propTypes = {
    createPost : propTypes.func.isRequired,
}
export default connect(null, {createPost})(PostForm);