import React, { Component } from 'react';
import './App.css';
import Posts from './components/posts';
import PostForm from './components/postform';
import { Provider } from 'react-redux';
import store from './store';
import SearchByModelDropDown from './components/searchbymodeldropdown';
import SearchByOther from './components/searchbyother';
import searchbyother from './components/searchbyother';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
      <div className="App">
        <SearchByOther />
        <header className="App-header">
        <PostForm />
        </header>
        <SearchByModelDropDown />
        <Posts />
      </div>
      </Provider>
    );
  }
}

export default App;
